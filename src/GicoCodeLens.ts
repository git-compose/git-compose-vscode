/**
 * Copyright (c) 2020 Timur Sultanaev All rights reserved.
 * Use of this source code is governed by a BSD-style
 * license that can be found in the LICENSE file.
 */

import * as vscode from 'vscode';
import { Gico } from './gico';

class GicoCodeLens extends vscode.CodeLens {
    file: string
    constructor(range: vscode.Range, file: string) {
        super(range)
        this.file = file
    }
}

class GicoBranchCodeLens extends GicoCodeLens {
    name: string
    for: 'branch'
    constructor(range: vscode.Range, file: string, name: string) {
        super(range, file)
        this.name = name
        this.for = 'branch'
    }
}

function isGicoBranchCodeLens(ob: vscode.CodeLens): ob is GicoBranchCodeLens {
    return 'name' in ob && 'for' in ob && ob['for'] === 'branch'
}

class GicoUpCodeLens extends GicoCodeLens {
    for: 'up'
    repo: string | undefined
    constructor(range: vscode.Range, file: string, repo?: string) {
        super(range, file)
        this.for = 'up'
        this.repo = repo
    }
}

function isGicoUpCodeLens(ob: vscode.CodeLens): ob is GicoUpCodeLens {
    return 'for' in ob && ob['for'] === 'up'
}

class GicoRootPushCodeLens extends GicoCodeLens {
    for: 'rootPush'
    constructor(range: vscode.Range, file: string) {
        super(range, file)
        this.for = 'rootPush'
    }
}

function isGicoRootPushCodeLens(ob: vscode.CodeLens): ob is GicoRootPushCodeLens {
    return 'for' in ob && ob['for'] === 'rootPush'
}

class GicoRootPullCodeLens extends GicoCodeLens {
    for: 'rootPull'
    constructor(range: vscode.Range, file: string) {
        super(range, file)
        this.for = 'rootPull'
    }
}

function isGicoRootPullCodeLens(ob: vscode.CodeLens): ob is GicoRootPullCodeLens {
    return 'for' in ob && ob['for'] === 'rootPull'
}

class GicoPullCodeLens extends GicoCodeLens {
    name?: string
    for: 'pull'
    constructor(range: vscode.Range, file: string, name?: string) {
        super(range, file)
        this.name = name
        this.for = 'pull'
    }
}

function isGicoPullCodeLens(ob: vscode.CodeLens): ob is GicoPullCodeLens {
    return 'name' in ob && 'for' in ob && ob['for'] === 'pull'
}

class GicoPushCodeLens extends GicoCodeLens {
    name?: string
    for: 'push'
    constructor(range: vscode.Range, file: string, name?: string) {
        super(range, file)
        this.name = name
        this.for = 'push'
    }
}

function isGicoPushCodeLens(ob: vscode.CodeLens): ob is GicoPushCodeLens {
    return 'name' in ob && 'for' in ob && ob['for'] === 'push'
}

class GicoOpenWindowCodeLens extends GicoCodeLens {
    path: string
    for: 'open'
    constructor(range: vscode.Range, file: string, path: string) {
        super(range, file)
        this.path = path
        this.for = 'open'
    }
}

function isGicoOpenWindowCodeLens(ob: vscode.CodeLens): ob is GicoOpenWindowCodeLens {
    return 'path' in ob && 'for' in ob && ob['for'] === 'open'
}

async function lens(gico: Gico, document: vscode.TextDocument): Promise<vscode.CodeLens[]> {
    const text = document.getText()
    const parsed = await gico.parse(text, document.fileName)
    //const parsed = await gico.parse(text)
    const forEachRepo = Object.entries(parsed.repos).flatMap(pair => {
        const name = pair[0]
        const repo = pair[1]
        const line = document.lineAt(repo.line - 1)
        const path = repo.path ? repo.path : name
        return [
            new GicoBranchCodeLens(line.range, document.fileName, name),
            new GicoUpCodeLens(line.range, document.fileName, name),
            new GicoPushCodeLens(line.range, document.fileName, name),
            new GicoOpenWindowCodeLens(line.range, document.fileName, path),
            // new GicoPullCodeLens(line.range, document.fileName, name),
            // pull not supported yet

        ]
    })
    const line = document.lineAt(parsed.metadata.reposLineNumber - 1)
    return [
        new GicoUpCodeLens(line.range, document.fileName),
        // new GicoRootPullCodeLens(line.range, document.fileName),
        // pull not supported yet
        new GicoRootPushCodeLens(line.range, document.fileName),
        ...forEachRepo
    ]
}

export const onDidChangeCodeLensesEmmiter = new vscode.EventEmitter<void>();

export default class GicoCodeLensProvider implements vscode.CodeLensProvider {
    private gico: Gico
    public onDidChangeCodeLenses?: vscode.Event<void>;

    constructor(gico: Gico) {
        this.gico = gico
        this.onDidChangeCodeLenses = onDidChangeCodeLensesEmmiter.event
    }

    public provideCodeLenses(document: vscode.TextDocument, token: vscode.CancellationToken):
        vscode.CodeLens[] | Thenable<vscode.CodeLens[]> {
        return lens(this.gico, document)
    }

    public resolveCodeLens(codeLens: vscode.CodeLens, token: vscode.CancellationToken):
        vscode.CodeLens | Thenable<vscode.CodeLens> {
        if (isGicoBranchCodeLens(codeLens)) {
            return this.gico.getBranches(codeLens.file).then(branches => {
                const branch = branches[codeLens.name]
                codeLens.command = {
                    title: `⎇ ${branch ? branch : "not cloned yet"}`,
                    tooltip: "",
                    command: "vscode-gico.repoCodeLens",
                    arguments: [codeLens.file]
                }
                return codeLens
            })
        }
        if (isGicoPullCodeLens(codeLens)) {
            codeLens.command = {
                title: `⭳ pull`,
                tooltip: "",
                command: "vscode-gico.repoCodeLens",
                arguments: [codeLens.file, codeLens.name]
            }
            return codeLens
        }
        if (isGicoPushCodeLens(codeLens)) {
            codeLens.command = {
                title: `↥ push`,
                tooltip: "",
                command: "gitCompose.reposPush",
                arguments: [codeLens.file, codeLens.name]
            }
            return codeLens
        }
        if (isGicoUpCodeLens(codeLens)) {
            codeLens.command = {
                title: `⟳ up`,
                tooltip: "Run to reach desired state - clone if not cloned, checkout if branch is different, pulls from remote",
                command: "gitCompose.reposUp",
                arguments: [codeLens.file, codeLens.repo]
            }
            return codeLens
        }
        if (isGicoRootPullCodeLens(codeLens)) {
            codeLens.command = {
                title: `⭳ pull`,
                tooltip: "",
                command: "gitCompose.reposPull",
                arguments: [codeLens.file]
            }
            return codeLens
        }
        if (isGicoRootPushCodeLens(codeLens)) {
            codeLens.command = {
                title: `↥ push`,
                tooltip: "",
                command: "gitCompose.reposPush",
                arguments: [codeLens.file]
            }
            return codeLens
        }
        if (isGicoOpenWindowCodeLens(codeLens)) {
            let url = vscode.Uri.file(`${vscode.workspace.rootPath}/${codeLens.path}`)
            codeLens.command = {
                title: `🡥 open`,
                tooltip: "",
                command: "vscode.openFolder",
                arguments: [url, true]
            }
            return codeLens
        }
        return codeLens
    }
}
