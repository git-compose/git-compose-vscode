/**
 * Copyright (c) 2020 Timur Sultanaev All rights reserved.
 * Use of this source code is governed by a BSD-style
 * license that can be found in the LICENSE file.
 */

import * as vscode from 'vscode';
import * as c_proc from 'child_process';
import * as util from 'util'
//import * as protoLoader from '@grpc/proto-loader'
import * as grpc from '@grpc/grpc-js'
import * as services from "./gen/gico_grpc_pb"
import * as messages from "./gen/gico_pb"
import * as os from 'os'
import * as fs from 'fs'

const execFile = util.promisify(c_proc.execFile)

interface RepoDef {
    line: number,
    path: string
}

type RepoDefs = { [key: string]: RepoDef }
interface Metadata {
    reposLineNumber: number
}

interface GitCompose {
    repos: RepoDefs
    metadata: Metadata
}

export class Gico {
    private context: vscode.ExtensionContext;
    public binaryPath = "gico";
    private proc: c_proc.ChildProcess;
    private client: services.GicoClient | undefined;
    private output: vscode.OutputChannel;
    private version: string;
    //private packageDefinition: protoLoader.PackageDefinition;
    //private packageObject: grpc.GrpcObject;


    private runProc(port: number) {
        return c_proc.execFile(
            this.binaryPath,
            ["serve", "--listen-port", `${port}`],
            { cwd: vscode.workspace.rootPath },
            (e) => {
                if (e) {
                    vscode.window.showErrorMessage(`failed to run gico server: ${e}, falling back to CLI`)
                    this.client?.close()
                    this.client = undefined
                }
            })
    }

    constructor(context: vscode.ExtensionContext,
        output: vscode.OutputChannel,
        port: number) {
        this.context = context;
        this.selectBinary();
        this.version = "unknown"
        this.output = output
        if (os.platform() == "linux" || os.platform() == "darwin") {
            c_proc.execSync(`chmod +x ${this.binaryPath}`)
        }
        this.proc = this.runProc(port)
        if (!this.proc.pid) {
            return
        }
        fs.mkdirSync(this.context.globalStoragePath, { recursive: true })
        const logfile = `${this.context.globalStoragePath}/.gico.log`
        fs.unlinkSync(logfile)
        console.log(`writing log to ${logfile}`)
        this.output.appendLine(`Started gico serve with PID ${this.proc.pid}`)
        // var bufOut: Uint8Array[] = [];
        this.proc.stderr.on('data', function (d) {
            // bufOut.push(d);
            fs.appendFile(logfile, Buffer.from(d), function (err) {
                if (err) {
                    console.log(`Cannot write log: ${err}`)
                }
            });
        });
        // this.proc.stderr.on('end', () => {
        //     var buf = Buffer.concat(bufOut)
        //     var out = buf.toString()
        //     console.error(`gico stream ended: ` + buf.toString())
        //     this.output.appendLine(`\u001b[31m gico stream ended:${out}\u001b[39m`)
        // })
        this.proc.once("exit", (code) => {
            if (code != 0) {
                vscode.window.showErrorMessage(`gico exited with code ${code}`)
                // var buf = Buffer.concat(bufOut)
                // var out = buf.toString()
                // console.error(`gico exited: ` + out)
                console.error(`gico exited: see logs in ${logfile}`)
            }
        })
        //this.packageDefinition = protoLoader.loadSync(this.context.extensionPath + "/gico.proto");
        //this.packageObject = grpc.loadPackageDefinition(this.packageDefinition);
        this.client = new services.GicoClient(`127.0.0.1:${port}`, grpc.credentials.createInsecure())
    }

    public async checkVersion() {
        return await new Promise<void>((resolve, reject) => {
            this.checkVersionRetry(0, resolve, reject)
        })
    }

    private checkVersionRetry(retry: number, resolve: () => void, reject: () => void) {
        var reqVersion = new messages.ReqVersion()
        if (retry > 0) {
            console.log(`${retry} retry to check gico version`)
        }
        if (!this.client) {
            console.error("no client to check gico version: " + this.client)
            resolve()
        }
        this.client?.getVersion(reqVersion, (e, res) => {
            if (e) {
                if (retry > 5) {
                    vscode.window.showWarningMessage(`failed to check server version: ${e}, falling back to use CLI`)
                    this.client?.close()
                    this.client = undefined
                    resolve()
                } else {
                    const msg = `failed to check server version: ${e}, retrying after 1 second: try ${retry}`
                    console.warn(msg)
                    this.output.appendLine(msg)
                    setTimeout(() => {
                        this.checkVersionRetry(retry + 1, resolve, reject)
                    }, 1000)
                }
            } else {
                const version = res.getNormal()
                this.version = version
                console.log("server version: " + version)
                this.output.appendLine("gico server version: " + version)
                if (!version.startsWith("0.") && !version.startsWith("1.")) {
                    vscode.window.showErrorMessage(
                        `Version of gico ${version} is not supported by this extension, 
                        check that versions are correlated, expected 0. or 1. version.`)
                }
                resolve()
            }
        })
    }

    private waitProcStopped(retries: number,
        resolve: (arg: string) => void,
        reject: (arg: string) => void) {
        //const noretries = 6
        //this.checkVersion(noretries)
        if (this.proc.killed) {
            const msg = "Gico process has been killed"
            console.log(msg)
            resolve(msg)
        } else if (retries > 3) {
            const msg = "Failed to wait till gico serve process is stopped, exiting"
            console.log(msg)
            resolve(msg)
        } else {
            setTimeout(() => {
                this.waitProcStopped(retries + 1, resolve, reject)
            }, 1000)
        }
    }

    public async shutdown() {
        if (this.proc.pid) {
            const msg = "Shutting gico server process down..."
            console.log(msg)
            this.output.appendLine(msg)
            this.proc.kill("SIGTERM")
            return await new Promise<string>((resolve, reject) => {
                this.waitProcStopped(0, resolve, reject)
            })
        } else {
            return "No process identifier to shut down gico"
        }
    }

    public async pull(file: string, repo?: string) {
        if (this.client) {
            const req = new messages.ReqRepo()
            req.setFile(file)
            if (repo) {
                req.setRepo(repo)
            }
            this.client.pull(req, (e, res) => {
                if (e) {
                    vscode.window.showErrorMessage(
                        `Failed to call "pull" on gico server ${this.version}: ${e}; fall back to CLI`)
                    this.client?.close()
                    this.client = undefined
                    this.pull(file, repo)
                }
            })
            return
        }
        const args = ["pull", "--file", file]
        if (repo) {
            args.push("--repo")
            args.push(repo)
        }
        const res = await execFile(this.binaryPath, args, { cwd: vscode.workspace.rootPath })
        return res.stdout.trim()
    }

    public async push(file: string, repo?: string) {
        if (this.client) {
            const req = new messages.ReqRepo()
            req.setFile(file)
            if (repo) {
                req.setRepo(repo)
            }
            this.client.push(req, (e, res) => {
                if (e) {
                    vscode.window.showErrorMessage(
                        `Failed to call "push" on gico server ${this.version}: ${e}; fall back to CLI`)
                    this.client?.close()
                    this.client = undefined
                    this.push(file, repo)
                }
            })
            return
        }
        const args = ["push", "--file", file]
        if (repo) {
            args.push("--repo")
            args.push(repo)
        }
        const res = await execFile(this.binaryPath, args, { cwd: vscode.workspace.rootPath })
        return res.stdout.trim()
    }

    public async up(file: string, repo?: string) {
        if (this.client) {
            const req = new messages.ReqRepo()
            req.setFile(file)
            if (repo) {
                req.setRepo(repo)
            }
            await vscode.window.withProgress<void>({
                title: "Running gico up...",
                cancellable: false,
                location: vscode.ProgressLocation.Window
            }, () => new Promise((resolve, reject) => {
                this.client?.up(req, (e, res) => {
                    if (e) {
                        vscode.window.showErrorMessage(
                            `Failed to call "up" on gico server ${this.version}: ${e}; fall back to CLI`)
                        this.client?.close()
                        this.client = undefined
                        this.up(file, repo)
                    }
                    resolve()
                })
            }))
            return
        }
        const args = ["up", "--file", file]
        if (repo) {
            args.push("--repo")
            args.push(repo)
        }
        const res = await execFile(this.binaryPath, args, { cwd: vscode.workspace.rootPath })
        return res.stdout.trim()
    }

    public async runInit(url?: string) {
        if (this.client) {
            const req = new messages.ReqInit()
            if (url) {
                req.setUrl(url)
            }
            this.client.init(req, (e, res) => {
                if (e) {
                    vscode.window.showErrorMessage(
                        `Failed to call "init" on gico server ${this.version}: ${e}; fall back to CLI`)
                    this.client?.close()
                    this.client = undefined
                    this.runInit(url)
                }
            })
            return
        }
        const args = ["init"]
        if (url) {
            args.push(url)
        }
        const res = await execFile(this.binaryPath, args, { cwd: vscode.workspace.rootPath })
        return res.stdout.trim()
    }

    public async getBranches(file: string): Promise<{ [key: string]: string }> {
        if (this.client) {
            const req = new messages.ReqBranches()
            if (file) {
                req.setFile(file)
            }
            try {
                let res = await new Promise<messages.ResBranches>((resolve, reject) => {
                    this.client?.getBranches(req, (e, res) => {
                        if (e) {
                            vscode.window.showErrorMessage(
                                `Failed to call "getBranches" on gico server ${this.version}: ${e}; fall back to CLI`)
                            this.client?.close()
                            this.client = undefined
                            reject(e)
                        } else {
                            resolve(res)
                        }
                    })
                    if (!this.client) {
                        reject(`Failed to call "getBranches" on gico server ${this.version}`)
                    }
                })
                let branchesList = res.getBranchesList()
                const object: { [key: string]: string } = {}
                branchesList.forEach((repoBranch) => {
                    object[repoBranch.getRepo()] = repoBranch.getBranch()
                })
                return object
            } catch (e) {
                console.log("error while receiving branches: ", e)
            }
        }

        const res = await execFile(this.binaryPath, ["branch", "--file", file], { cwd: vscode.workspace.rootPath })
        const text = res.stdout.trim()
        const object: { [key: string]: string } = {}
        text.split("\n").forEach(line => {
            const pair = line.split(" ")
            if (pair.length > 1) {
                object[pair[0]] = pair[1]
            }
        })
        return object
    }

    public async parse(input: string, file?: string): Promise<GitCompose> {
        return new Promise((resolve, reject) => {
            if (this.client /*&& file*/) {
                const req = new messages.ReqFile()
                // req.setFile(file)
                
                req.setData(input)

                this.client.parseFile(req, (e, res) => {
                    if (e) {
                        vscode.window.showErrorMessage(
                            `Failed to call "parseFile" on gico server ${this.version}: ${e}; fall back to CLI`)
                        this.client?.close()
                        this.client = undefined
                        reject(`Failed to call "parseFile" on gico server`)
                    } else {
                        const num = res.getMetadata()?.getReposlinenumber()
                        const result: GitCompose = {
                            metadata: {
                                reposLineNumber: num ? num : 0
                            },
                            repos: {}
                        }
                        res.getReposMap().forEach((repoDef, name) => {
                            result.repos[name] = {
                                line: repoDef.getLine(),
                                path: repoDef.getPath()
                            }
                        })
                        resolve(result)
                    }
                })
                return
            }

            const proc = c_proc.execFile(this.binaryPath, ["parse", "-f", "-"], { cwd: vscode.workspace.rootPath })
            var bufs: Buffer[] = [];
            proc.stdout.on('data', function (d: string) {
                bufs.push(Buffer.from(d, 'utf8'));
            });
            proc.stdout.on('end', function () {
                const buf = Buffer.concat(bufs)
                const compose: GitCompose = JSON.parse(buf.toString())
                resolve(compose)
            })
            proc.stdin.write(input)
            proc.stdin.end()
            proc.once("exit", (code) => {
                if (code != 0) {
                    reject("exited with code " + code)
                }
            })
        })
    }

    private getOSBundledExe() {
        switch (process.platform) {
            case "win32":
            case "cygwin":
                return "windows/gico.exe"
            case "linux":
                return "linux/gico"
            case "darwin":
                return "macos/gico.mac.exe"
            default:
                throw Error("unknown platform: " + process.platform + " , don't have bundled gico build for it, switch to system gico")
        }
    }

    private selectBinary() {
        const useBundled = vscode.workspace.getConfiguration('gitCompose').get('useBundledGico')
        if (useBundled) {
            this.binaryPath = this.context.extensionPath + "/bundled-gico/" + this.getOSBundledExe()
        } else {
            console.log("using system gico")
        }
    }
}
