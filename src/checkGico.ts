/**
 * Copyright (c) 2020 Timur Sultanaev All rights reserved.
 * Use of this source code is governed by a BSD-style
 * license that can be found in the LICENSE file.
 */

import * as vscode from 'vscode';
import * as c_proc from 'child_process';
import * as util from 'util'
import { Gico } from './gico';

const execFile = util.promisify(c_proc.execFile)

const execGico: (args: string[], stdin?: string) => Promise<string> = (args: string[], stdin?: string) => {
    return new Promise((resolve, reject) => {
        const proc: c_proc.ChildProcess = c_proc.execFile('gico', args, { cwd: vscode.workspace.rootPath })
        if (stdin) {
            proc.stdin.write(stdin)
            proc.stdin.end()
        }

        var bufOut: Uint8Array[] = [];
        proc.stdout.on('data', function (d) { bufOut.push(d); });
        proc.stdout.on('end', function () {
            var buf = Buffer.concat(bufOut);
            resolve(buf.toString())
        })
        proc.once("exit", (code) => {
            if (code != 0) {
                reject("exited with code " + code)
            }
        })
    })
}

export default async (gico: Gico) => {
    try {
        console.log("Checking version")
        const res = await execFile(gico.binaryPath, ["version", "--normal"], { cwd: vscode.workspace.rootPath })
        const version = res.stdout.trim()
        console.log("got version: " + version)
        if (version.length < 1) {
            vscode.window.showErrorMessage(
                `Version of gico (empty) is not supported by this extension, 
                check that correct gico is installed.`)
            return
            // TODO parse semver, add more detailed compatibility check, add link
            // to compatibility documentation
        }

        if (!version.startsWith("0.") && !version.startsWith("1.")) {
            vscode.window.showErrorMessage(
                `Version of gico ${version} is not supported by this extension, 
                check that versions are correlated, expected 0. or 1. version.`)
        }
        return version
    } catch (e) {
        vscode.window.showErrorMessage(
            `Looks like you don't have gico setup properly..
			You can read how to install it for your system
			[in the documentation here](https://gitlab.com/git-compose/git-compose).
			
			Error details: ${e}`)
    }
}