/**
 * Copyright (c) 2020 Timur Sultanaev All rights reserved.
 * Use of this source code is governed by a BSD-style
 * license that can be found in the LICENSE file.
 */

import * as vscode from 'vscode';
import * as fs from 'fs'

export default async (context: vscode.ExtensionContext) => {
    const exampleListRepos = (await fs.promises.readFile(context.extensionPath + "/examples/list-repos.yaml", "utf8")).toString()
	return vscode.languages.registerHoverProvider('yaml', {
		provideHover(document, position, token) {
			if (document.fileName.endsWith("git-compose.yaml")) {

				const lineStart = new vscode.Position(position.line, 0)
				const maxSearch = new vscode.Position(position.line, position.character + 5)

				const text = document.getText(new vscode.Range(lineStart, maxSearch))
				if (text.startsWith("repos:")) {
					return new vscode.Hover(
						new vscode.MarkdownString('List of git repositories, for example:')
							.appendCodeblock(exampleListRepos.trimLeft())
					);
				}
			}
			return;
		}
	});
}