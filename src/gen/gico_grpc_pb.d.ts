// package: gico
// file: gico.proto

/* tslint:disable */
/* eslint-disable */

import * as grpc from "@grpc/grpc-js";
import {handleClientStreamingCall} from "@grpc/grpc-js/build/src/server-call";
import * as gico_pb from "./gico_pb";

interface IGicoService extends grpc.ServiceDefinition<grpc.UntypedServiceImplementation> {
    getVersion: IGicoService_IGetVersion;
    up: IGicoService_IUp;
    pull: IGicoService_IPull;
    push: IGicoService_IPush;
    init: IGicoService_IInit;
    getBranches: IGicoService_IGetBranches;
    parseFile: IGicoService_IParseFile;
}

interface IGicoService_IGetVersion extends grpc.MethodDefinition<gico_pb.ReqVersion, gico_pb.Version> {
    path: string; // "/gico.Gico/GetVersion"
    requestStream: false;
    responseStream: false;
    requestSerialize: grpc.serialize<gico_pb.ReqVersion>;
    requestDeserialize: grpc.deserialize<gico_pb.ReqVersion>;
    responseSerialize: grpc.serialize<gico_pb.Version>;
    responseDeserialize: grpc.deserialize<gico_pb.Version>;
}
interface IGicoService_IUp extends grpc.MethodDefinition<gico_pb.ReqRepo, gico_pb.Res> {
    path: string; // "/gico.Gico/Up"
    requestStream: false;
    responseStream: false;
    requestSerialize: grpc.serialize<gico_pb.ReqRepo>;
    requestDeserialize: grpc.deserialize<gico_pb.ReqRepo>;
    responseSerialize: grpc.serialize<gico_pb.Res>;
    responseDeserialize: grpc.deserialize<gico_pb.Res>;
}
interface IGicoService_IPull extends grpc.MethodDefinition<gico_pb.ReqRepo, gico_pb.Res> {
    path: string; // "/gico.Gico/Pull"
    requestStream: false;
    responseStream: false;
    requestSerialize: grpc.serialize<gico_pb.ReqRepo>;
    requestDeserialize: grpc.deserialize<gico_pb.ReqRepo>;
    responseSerialize: grpc.serialize<gico_pb.Res>;
    responseDeserialize: grpc.deserialize<gico_pb.Res>;
}
interface IGicoService_IPush extends grpc.MethodDefinition<gico_pb.ReqRepo, gico_pb.Res> {
    path: string; // "/gico.Gico/Push"
    requestStream: false;
    responseStream: false;
    requestSerialize: grpc.serialize<gico_pb.ReqRepo>;
    requestDeserialize: grpc.deserialize<gico_pb.ReqRepo>;
    responseSerialize: grpc.serialize<gico_pb.Res>;
    responseDeserialize: grpc.deserialize<gico_pb.Res>;
}
interface IGicoService_IInit extends grpc.MethodDefinition<gico_pb.ReqInit, gico_pb.Res> {
    path: string; // "/gico.Gico/Init"
    requestStream: false;
    responseStream: false;
    requestSerialize: grpc.serialize<gico_pb.ReqInit>;
    requestDeserialize: grpc.deserialize<gico_pb.ReqInit>;
    responseSerialize: grpc.serialize<gico_pb.Res>;
    responseDeserialize: grpc.deserialize<gico_pb.Res>;
}
interface IGicoService_IGetBranches extends grpc.MethodDefinition<gico_pb.ReqBranches, gico_pb.ResBranches> {
    path: string; // "/gico.Gico/GetBranches"
    requestStream: false;
    responseStream: false;
    requestSerialize: grpc.serialize<gico_pb.ReqBranches>;
    requestDeserialize: grpc.deserialize<gico_pb.ReqBranches>;
    responseSerialize: grpc.serialize<gico_pb.ResBranches>;
    responseDeserialize: grpc.deserialize<gico_pb.ResBranches>;
}
interface IGicoService_IParseFile extends grpc.MethodDefinition<gico_pb.ReqFile, gico_pb.ComposeFile> {
    path: string; // "/gico.Gico/ParseFile"
    requestStream: false;
    responseStream: false;
    requestSerialize: grpc.serialize<gico_pb.ReqFile>;
    requestDeserialize: grpc.deserialize<gico_pb.ReqFile>;
    responseSerialize: grpc.serialize<gico_pb.ComposeFile>;
    responseDeserialize: grpc.deserialize<gico_pb.ComposeFile>;
}

export const GicoService: IGicoService;

export interface IGicoServer {
    getVersion: grpc.handleUnaryCall<gico_pb.ReqVersion, gico_pb.Version>;
    up: grpc.handleUnaryCall<gico_pb.ReqRepo, gico_pb.Res>;
    pull: grpc.handleUnaryCall<gico_pb.ReqRepo, gico_pb.Res>;
    push: grpc.handleUnaryCall<gico_pb.ReqRepo, gico_pb.Res>;
    init: grpc.handleUnaryCall<gico_pb.ReqInit, gico_pb.Res>;
    getBranches: grpc.handleUnaryCall<gico_pb.ReqBranches, gico_pb.ResBranches>;
    parseFile: grpc.handleUnaryCall<gico_pb.ReqFile, gico_pb.ComposeFile>;
}

export interface IGicoClient {
    getVersion(request: gico_pb.ReqVersion, callback: (error: grpc.ServiceError | null, response: gico_pb.Version) => void): grpc.ClientUnaryCall;
    getVersion(request: gico_pb.ReqVersion, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: gico_pb.Version) => void): grpc.ClientUnaryCall;
    getVersion(request: gico_pb.ReqVersion, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: gico_pb.Version) => void): grpc.ClientUnaryCall;
    up(request: gico_pb.ReqRepo, callback: (error: grpc.ServiceError | null, response: gico_pb.Res) => void): grpc.ClientUnaryCall;
    up(request: gico_pb.ReqRepo, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: gico_pb.Res) => void): grpc.ClientUnaryCall;
    up(request: gico_pb.ReqRepo, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: gico_pb.Res) => void): grpc.ClientUnaryCall;
    pull(request: gico_pb.ReqRepo, callback: (error: grpc.ServiceError | null, response: gico_pb.Res) => void): grpc.ClientUnaryCall;
    pull(request: gico_pb.ReqRepo, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: gico_pb.Res) => void): grpc.ClientUnaryCall;
    pull(request: gico_pb.ReqRepo, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: gico_pb.Res) => void): grpc.ClientUnaryCall;
    push(request: gico_pb.ReqRepo, callback: (error: grpc.ServiceError | null, response: gico_pb.Res) => void): grpc.ClientUnaryCall;
    push(request: gico_pb.ReqRepo, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: gico_pb.Res) => void): grpc.ClientUnaryCall;
    push(request: gico_pb.ReqRepo, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: gico_pb.Res) => void): grpc.ClientUnaryCall;
    init(request: gico_pb.ReqInit, callback: (error: grpc.ServiceError | null, response: gico_pb.Res) => void): grpc.ClientUnaryCall;
    init(request: gico_pb.ReqInit, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: gico_pb.Res) => void): grpc.ClientUnaryCall;
    init(request: gico_pb.ReqInit, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: gico_pb.Res) => void): grpc.ClientUnaryCall;
    getBranches(request: gico_pb.ReqBranches, callback: (error: grpc.ServiceError | null, response: gico_pb.ResBranches) => void): grpc.ClientUnaryCall;
    getBranches(request: gico_pb.ReqBranches, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: gico_pb.ResBranches) => void): grpc.ClientUnaryCall;
    getBranches(request: gico_pb.ReqBranches, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: gico_pb.ResBranches) => void): grpc.ClientUnaryCall;
    parseFile(request: gico_pb.ReqFile, callback: (error: grpc.ServiceError | null, response: gico_pb.ComposeFile) => void): grpc.ClientUnaryCall;
    parseFile(request: gico_pb.ReqFile, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: gico_pb.ComposeFile) => void): grpc.ClientUnaryCall;
    parseFile(request: gico_pb.ReqFile, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: gico_pb.ComposeFile) => void): grpc.ClientUnaryCall;
}

export class GicoClient extends grpc.Client implements IGicoClient {
    constructor(address: string, credentials: grpc.ChannelCredentials, options?: Partial<grpc.ClientOptions>);
    public getVersion(request: gico_pb.ReqVersion, callback: (error: grpc.ServiceError | null, response: gico_pb.Version) => void): grpc.ClientUnaryCall;
    public getVersion(request: gico_pb.ReqVersion, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: gico_pb.Version) => void): grpc.ClientUnaryCall;
    public getVersion(request: gico_pb.ReqVersion, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: gico_pb.Version) => void): grpc.ClientUnaryCall;
    public up(request: gico_pb.ReqRepo, callback: (error: grpc.ServiceError | null, response: gico_pb.Res) => void): grpc.ClientUnaryCall;
    public up(request: gico_pb.ReqRepo, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: gico_pb.Res) => void): grpc.ClientUnaryCall;
    public up(request: gico_pb.ReqRepo, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: gico_pb.Res) => void): grpc.ClientUnaryCall;
    public pull(request: gico_pb.ReqRepo, callback: (error: grpc.ServiceError | null, response: gico_pb.Res) => void): grpc.ClientUnaryCall;
    public pull(request: gico_pb.ReqRepo, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: gico_pb.Res) => void): grpc.ClientUnaryCall;
    public pull(request: gico_pb.ReqRepo, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: gico_pb.Res) => void): grpc.ClientUnaryCall;
    public push(request: gico_pb.ReqRepo, callback: (error: grpc.ServiceError | null, response: gico_pb.Res) => void): grpc.ClientUnaryCall;
    public push(request: gico_pb.ReqRepo, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: gico_pb.Res) => void): grpc.ClientUnaryCall;
    public push(request: gico_pb.ReqRepo, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: gico_pb.Res) => void): grpc.ClientUnaryCall;
    public init(request: gico_pb.ReqInit, callback: (error: grpc.ServiceError | null, response: gico_pb.Res) => void): grpc.ClientUnaryCall;
    public init(request: gico_pb.ReqInit, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: gico_pb.Res) => void): grpc.ClientUnaryCall;
    public init(request: gico_pb.ReqInit, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: gico_pb.Res) => void): grpc.ClientUnaryCall;
    public getBranches(request: gico_pb.ReqBranches, callback: (error: grpc.ServiceError | null, response: gico_pb.ResBranches) => void): grpc.ClientUnaryCall;
    public getBranches(request: gico_pb.ReqBranches, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: gico_pb.ResBranches) => void): grpc.ClientUnaryCall;
    public getBranches(request: gico_pb.ReqBranches, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: gico_pb.ResBranches) => void): grpc.ClientUnaryCall;
    public parseFile(request: gico_pb.ReqFile, callback: (error: grpc.ServiceError | null, response: gico_pb.ComposeFile) => void): grpc.ClientUnaryCall;
    public parseFile(request: gico_pb.ReqFile, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: gico_pb.ComposeFile) => void): grpc.ClientUnaryCall;
    public parseFile(request: gico_pb.ReqFile, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: gico_pb.ComposeFile) => void): grpc.ClientUnaryCall;
}
