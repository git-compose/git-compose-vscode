// GENERATED CODE -- DO NOT EDIT!

// Original file comments:
// Copyright (c) 2020 Timur Sultanaev All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.
//
'use strict';
var grpc = require('@grpc/grpc-js');
var gico_pb = require('./gico_pb.js');

function serialize_gico_ComposeFile(arg) {
  if (!(arg instanceof gico_pb.ComposeFile)) {
    throw new Error('Expected argument of type gico.ComposeFile');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_gico_ComposeFile(buffer_arg) {
  return gico_pb.ComposeFile.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_gico_ReqBranches(arg) {
  if (!(arg instanceof gico_pb.ReqBranches)) {
    throw new Error('Expected argument of type gico.ReqBranches');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_gico_ReqBranches(buffer_arg) {
  return gico_pb.ReqBranches.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_gico_ReqFile(arg) {
  if (!(arg instanceof gico_pb.ReqFile)) {
    throw new Error('Expected argument of type gico.ReqFile');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_gico_ReqFile(buffer_arg) {
  return gico_pb.ReqFile.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_gico_ReqInit(arg) {
  if (!(arg instanceof gico_pb.ReqInit)) {
    throw new Error('Expected argument of type gico.ReqInit');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_gico_ReqInit(buffer_arg) {
  return gico_pb.ReqInit.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_gico_ReqRepo(arg) {
  if (!(arg instanceof gico_pb.ReqRepo)) {
    throw new Error('Expected argument of type gico.ReqRepo');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_gico_ReqRepo(buffer_arg) {
  return gico_pb.ReqRepo.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_gico_ReqVersion(arg) {
  if (!(arg instanceof gico_pb.ReqVersion)) {
    throw new Error('Expected argument of type gico.ReqVersion');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_gico_ReqVersion(buffer_arg) {
  return gico_pb.ReqVersion.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_gico_Res(arg) {
  if (!(arg instanceof gico_pb.Res)) {
    throw new Error('Expected argument of type gico.Res');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_gico_Res(buffer_arg) {
  return gico_pb.Res.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_gico_ResBranches(arg) {
  if (!(arg instanceof gico_pb.ResBranches)) {
    throw new Error('Expected argument of type gico.ResBranches');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_gico_ResBranches(buffer_arg) {
  return gico_pb.ResBranches.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_gico_Version(arg) {
  if (!(arg instanceof gico_pb.Version)) {
    throw new Error('Expected argument of type gico.Version');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_gico_Version(buffer_arg) {
  return gico_pb.Version.deserializeBinary(new Uint8Array(buffer_arg));
}


var GicoService = exports.GicoService = {
  getVersion: {
    path: '/gico.Gico/GetVersion',
    requestStream: false,
    responseStream: false,
    requestType: gico_pb.ReqVersion,
    responseType: gico_pb.Version,
    requestSerialize: serialize_gico_ReqVersion,
    requestDeserialize: deserialize_gico_ReqVersion,
    responseSerialize: serialize_gico_Version,
    responseDeserialize: deserialize_gico_Version,
  },
  up: {
    path: '/gico.Gico/Up',
    requestStream: false,
    responseStream: false,
    requestType: gico_pb.ReqRepo,
    responseType: gico_pb.Res,
    requestSerialize: serialize_gico_ReqRepo,
    requestDeserialize: deserialize_gico_ReqRepo,
    responseSerialize: serialize_gico_Res,
    responseDeserialize: deserialize_gico_Res,
  },
  pull: {
    path: '/gico.Gico/Pull',
    requestStream: false,
    responseStream: false,
    requestType: gico_pb.ReqRepo,
    responseType: gico_pb.Res,
    requestSerialize: serialize_gico_ReqRepo,
    requestDeserialize: deserialize_gico_ReqRepo,
    responseSerialize: serialize_gico_Res,
    responseDeserialize: deserialize_gico_Res,
  },
  push: {
    path: '/gico.Gico/Push',
    requestStream: false,
    responseStream: false,
    requestType: gico_pb.ReqRepo,
    responseType: gico_pb.Res,
    requestSerialize: serialize_gico_ReqRepo,
    requestDeserialize: deserialize_gico_ReqRepo,
    responseSerialize: serialize_gico_Res,
    responseDeserialize: deserialize_gico_Res,
  },
  init: {
    path: '/gico.Gico/Init',
    requestStream: false,
    responseStream: false,
    requestType: gico_pb.ReqInit,
    responseType: gico_pb.Res,
    requestSerialize: serialize_gico_ReqInit,
    requestDeserialize: deserialize_gico_ReqInit,
    responseSerialize: serialize_gico_Res,
    responseDeserialize: deserialize_gico_Res,
  },
  getBranches: {
    path: '/gico.Gico/GetBranches',
    requestStream: false,
    responseStream: false,
    requestType: gico_pb.ReqBranches,
    responseType: gico_pb.ResBranches,
    requestSerialize: serialize_gico_ReqBranches,
    requestDeserialize: deserialize_gico_ReqBranches,
    responseSerialize: serialize_gico_ResBranches,
    responseDeserialize: deserialize_gico_ResBranches,
  },
  parseFile: {
    path: '/gico.Gico/ParseFile',
    requestStream: false,
    responseStream: false,
    requestType: gico_pb.ReqFile,
    responseType: gico_pb.ComposeFile,
    requestSerialize: serialize_gico_ReqFile,
    requestDeserialize: deserialize_gico_ReqFile,
    responseSerialize: serialize_gico_ComposeFile,
    responseDeserialize: deserialize_gico_ComposeFile,
  },
};

exports.GicoClient = grpc.makeGenericClientConstructor(GicoService);
