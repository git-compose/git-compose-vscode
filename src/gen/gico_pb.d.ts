/**
 * Copyright (c) 2021 Timur Sultanaev All rights reserved.
 * Use of this source code is governed by a BSD-style
 * license that can be found in the LICENSE file.
 */

// package: gico
// file: gico.proto

/* tslint:disable */
/* eslint-disable */

import * as jspb from "google-protobuf";

export class RepoDef extends jspb.Message { 
    getLine(): number;
    setLine(value: number): RepoDef;

    getPath(): string;
    setPath(value: string): RepoDef;


    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): RepoDef.AsObject;
    static toObject(includeInstance: boolean, msg: RepoDef): RepoDef.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: RepoDef, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): RepoDef;
    static deserializeBinaryFromReader(message: RepoDef, reader: jspb.BinaryReader): RepoDef;
}

export namespace RepoDef {
    export type AsObject = {
        line: number,
        path: string,
    }
}

export class Metadata extends jspb.Message { 
    getReposlinenumber(): number;
    setReposlinenumber(value: number): Metadata;


    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Metadata.AsObject;
    static toObject(includeInstance: boolean, msg: Metadata): Metadata.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Metadata, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Metadata;
    static deserializeBinaryFromReader(message: Metadata, reader: jspb.BinaryReader): Metadata;
}

export namespace Metadata {
    export type AsObject = {
        reposlinenumber: number,
    }
}

export class ComposeFile extends jspb.Message { 

    getReposMap(): jspb.Map<string, RepoDef>;
    clearReposMap(): void;


    hasMetadata(): boolean;
    clearMetadata(): void;
    getMetadata(): Metadata | undefined;
    setMetadata(value?: Metadata): ComposeFile;


    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): ComposeFile.AsObject;
    static toObject(includeInstance: boolean, msg: ComposeFile): ComposeFile.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: ComposeFile, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): ComposeFile;
    static deserializeBinaryFromReader(message: ComposeFile, reader: jspb.BinaryReader): ComposeFile;
}

export namespace ComposeFile {
    export type AsObject = {

        reposMap: Array<[string, RepoDef.AsObject]>,
        metadata?: Metadata.AsObject,
    }
}

export class ReqFile extends jspb.Message { 
    getFile(): string;
    setFile(value: string): ReqFile;

    getData(): string;
    setData(value: string): ReqFile;


    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): ReqFile.AsObject;
    static toObject(includeInstance: boolean, msg: ReqFile): ReqFile.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: ReqFile, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): ReqFile;
    static deserializeBinaryFromReader(message: ReqFile, reader: jspb.BinaryReader): ReqFile;
}

export namespace ReqFile {
    export type AsObject = {
        file: string,
        data: string,
    }
}

export class RepoBranch extends jspb.Message { 
    getRepo(): string;
    setRepo(value: string): RepoBranch;

    getBranch(): string;
    setBranch(value: string): RepoBranch;


    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): RepoBranch.AsObject;
    static toObject(includeInstance: boolean, msg: RepoBranch): RepoBranch.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: RepoBranch, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): RepoBranch;
    static deserializeBinaryFromReader(message: RepoBranch, reader: jspb.BinaryReader): RepoBranch;
}

export namespace RepoBranch {
    export type AsObject = {
        repo: string,
        branch: string,
    }
}

export class ResBranches extends jspb.Message { 
    clearBranchesList(): void;
    getBranchesList(): Array<RepoBranch>;
    setBranchesList(value: Array<RepoBranch>): ResBranches;
    addBranches(value?: RepoBranch, index?: number): RepoBranch;


    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): ResBranches.AsObject;
    static toObject(includeInstance: boolean, msg: ResBranches): ResBranches.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: ResBranches, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): ResBranches;
    static deserializeBinaryFromReader(message: ResBranches, reader: jspb.BinaryReader): ResBranches;
}

export namespace ResBranches {
    export type AsObject = {
        branchesList: Array<RepoBranch.AsObject>,
    }
}

export class ReqBranches extends jspb.Message { 
    getFile(): string;
    setFile(value: string): ReqBranches;


    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): ReqBranches.AsObject;
    static toObject(includeInstance: boolean, msg: ReqBranches): ReqBranches.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: ReqBranches, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): ReqBranches;
    static deserializeBinaryFromReader(message: ReqBranches, reader: jspb.BinaryReader): ReqBranches;
}

export namespace ReqBranches {
    export type AsObject = {
        file: string,
    }
}

export class Res extends jspb.Message { 

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Res.AsObject;
    static toObject(includeInstance: boolean, msg: Res): Res.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Res, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Res;
    static deserializeBinaryFromReader(message: Res, reader: jspb.BinaryReader): Res;
}

export namespace Res {
    export type AsObject = {
    }
}

export class ReqInit extends jspb.Message { 
    getUrl(): string;
    setUrl(value: string): ReqInit;


    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): ReqInit.AsObject;
    static toObject(includeInstance: boolean, msg: ReqInit): ReqInit.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: ReqInit, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): ReqInit;
    static deserializeBinaryFromReader(message: ReqInit, reader: jspb.BinaryReader): ReqInit;
}

export namespace ReqInit {
    export type AsObject = {
        url: string,
    }
}

export class ReqRepo extends jspb.Message { 
    getRepo(): string;
    setRepo(value: string): ReqRepo;

    getFile(): string;
    setFile(value: string): ReqRepo;


    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): ReqRepo.AsObject;
    static toObject(includeInstance: boolean, msg: ReqRepo): ReqRepo.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: ReqRepo, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): ReqRepo;
    static deserializeBinaryFromReader(message: ReqRepo, reader: jspb.BinaryReader): ReqRepo;
}

export namespace ReqRepo {
    export type AsObject = {
        repo: string,
        file: string,
    }
}

export class ReqVersion extends jspb.Message { 

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): ReqVersion.AsObject;
    static toObject(includeInstance: boolean, msg: ReqVersion): ReqVersion.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: ReqVersion, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): ReqVersion;
    static deserializeBinaryFromReader(message: ReqVersion, reader: jspb.BinaryReader): ReqVersion;
}

export namespace ReqVersion {
    export type AsObject = {
    }
}

export class Version extends jspb.Message { 
    getNormal(): string;
    setNormal(value: string): Version;


    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Version.AsObject;
    static toObject(includeInstance: boolean, msg: Version): Version.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Version, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Version;
    static deserializeBinaryFromReader(message: Version, reader: jspb.BinaryReader): Version;
}

export namespace Version {
    export type AsObject = {
        normal: string,
    }
}
