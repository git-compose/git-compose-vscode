/**
 * Copyright (c) 2020 Timur Sultanaev All rights reserved.
 * Use of this source code is governed by a BSD-style
 * license that can be found in the LICENSE file.
 */

import * as vscode from 'vscode'
import checkGico from './checkGico'
import hoverHelp from './hoverHelp'
import GicoCodeLens, { onDidChangeCodeLensesEmmiter } from './GicoCodeLens'
import { Gico } from './gico'
import * as fs from 'fs'
import * as net from 'net'

var stop: () => Promise<string> = async () => {
	return "Running mock gico stop"
}

function getRandomInt(min: number, max: number) {
	min = Math.ceil(min);
	max = Math.floor(max);
	return Math.floor(Math.random() * (max - min) + min); //The maximum is exclusive and the minimum is inclusive
}

function checkPort(port: number, callback: (portTaken: boolean) => void) {
	var server = net.createServer(function (socket) {
		socket.write('Echo server\r\n');
		socket.pipe(socket);
	});

	server.listen(port, '127.0.0.1');
	server.on('error', function (e) {
		callback(true);
	});
	server.on('listening', function () {
		server.close();
		callback(false);
	});
}

function findPortAndRun(
	tries: number,
	runGico: (port: number) => void,
	reject: () => void) {
	const port = getRandomInt(1000, 3000)
	console.log("Trying to run `gico serve` on port: "+ port)
	checkPort(port, (portTaken: boolean) => {
		if (!portTaken) {
			runGico(port)
		} else if (tries > 0) {
			findPortAndRun(tries - 1, runGico, reject)
		} else {
			reject()
		}
	})
}
function runGicoProc(
	context: vscode.ExtensionContext,
	output: vscode.OutputChannel,
): Promise<Gico> {
	return new Promise<Gico>((resolve, reject) => {
		findPortAndRun(100, (port) => {
			const gico = new Gico(context, output, port)
			resolve(gico)
		}, () => { reject() })
	})
}

export async function activate(context: vscode.ExtensionContext) {
	const output = vscode.window.createOutputChannel("Gico")
	output.appendLine("Activating Gico extension...")
	const gico = await runGicoProc(context, output)
	await gico.checkVersion()
	stop = async () => {
		const shutted = await gico.shutdown()
		return shutted
	}
	//const version = await checkGico(gico)
	//if (!version) {
	//	return
	//}
	//console.log(`Using gico of version ${version}`)

	context.subscriptions.push(
		vscode.commands.registerCommand('vscode-gico.repoCodeLens', (file: string) => {
			vscode.window.showInformationMessage(`VCS integration coming soon!`);
			// TODO VCS integration, huh
		})
	)
	context.subscriptions.push(
		vscode.commands.registerCommand('gitCompose.init', async () => {
			const url = await vscode.window.showInputBox({ prompt: "Optional URL of remote git-compose file" })
			await gico.runInit(url)
		})
	)
	context.subscriptions.push(await hoverHelp(context));

	const gicoCodeLens = new GicoCodeLens(gico)

	context.subscriptions.push(
		vscode.languages.registerCodeLensProvider({
			language: "yaml",
			scheme: "file",
			pattern: "**/{git-compose.yaml,*.git-compose.yaml}",
		}, gicoCodeLens)
	)


	context.subscriptions.push(
		vscode.commands.registerCommand('gitCompose.reposUp', async (file: string, repo?: string) => {
			await gico.up(file, repo)
			onDidChangeCodeLensesEmmiter.fire()
		})
	)

	context.subscriptions.push(
		vscode.commands.registerCommand('gitCompose.reposPush', async (file: string, repo?: string) => {
			await gico.push(file, repo)
			onDidChangeCodeLensesEmmiter.fire()
		})
	)

	context.subscriptions.push(
		vscode.commands.registerCommand('gitCompose.reposPull', async (file: string, repo?: string) => {
			await gico.pull(file, repo)
			onDidChangeCodeLensesEmmiter.fire()
		})
	)

}

export async function deactivate() {
	return await stop()
}
