# git-compose

[Compose for Git](https://git-compose.gitlab.io/) is a tool to manage separate local git repositories at once. This extension integrates VSCode with Compose for Git `gico` CLI tool.

## Features

Any file named `git-compose.yaml` or ending with `.git-compose.yaml` would 
be processed.

### Lens

Current branch and control commands are shown as code lens

![git-compose-lens](images/lens.gif)

- `up` would try to clone if no folder is present, pull if present and would checkout is branch is different
- `push` is just usual git push
- `open` would open new vs code window 

## Requirements

Should have `git` installed and available via PATH.

## Extension Settings

### useBundledGico

Set `useBundledGico` to `false` if you want to use system binary instead of bundled.

## Known Issues

 - branch lens are not clickable, VCS support coming soon
 - after change in file some of lenses produce 'actual command NOT FOUND' ( https://github.com/microsoft/vscode/issues/96783 ), that can be solved with double cntrl+tab (switch to another file and back)
 - server panics in case if file was deleted, but operation is in progress, also when clone is in progress and branches are requested like this:
 
```
 2021/03/03 23:23:39 Starting server...
2021/03/03 23:23:44 Checking directory .vscode for git repository
2021/03/03 23:24:27 git-compose folder does not exist, cloning
2021/03/03 23:24:27 Upping git-compose
2021/03/03 23:24:28 Cannot get repository head: reference not found, fallback to CLI
fatal: ambiguous argument 'HEAD': unknown revision or path not in the working tree.
Use '--' to separate paths from revisions, like this:
'git <command> [<revision>...] -- [<file>...]'
panic: exit status 128

goroutine 53 [running]:
gitlab.com/git-compose/git-compose/internal/run.getCurrentBranchByCli(0xc0002af950, 0xb, 0xc00029b798, 0x1)
	/builds/git-compose/git-compose/internal/run/branch.go:77 +0x26c
gitlab.com/git-compose/git-compose/internal/run.getCurrentBranch(0xc0002af950, 0xb, 0xc00029b800, 0x0)
	/builds/git-compose/git-compose/internal/run/branch.go:40 +0x208
gitlab.com/git-compose/git-compose/internal/run.GetCurrentBranches(0xc00026f380, 0x2d)
	/builds/git-compose/git-compose/internal/run/branch.go:89 +0x196
gitlab.com/git-compose/git-compose/internal/server.(*gicoServer).GetBranches(0xc000058030, 0xd9f900, 0xc000382cc0, 0xc00026f340, 0xc000058030, 0xc000382cc0, 0xc000201bf8)
	/builds/git-compose/git-compose/internal/server/serve.go:98 +0x62
gitlab.com/git-compose/git-compose/gico_api._Gico_GetBranches_Handler(0xc19360, 0xc000058030, 0xd9f900, 0xc000382cc0, 0xc0002c1440, 0x0, 0xd9f900, 0xc000382cc0, 0xc0002d3950, 0x2f)
	/builds/git-compose/git-compose/gico_api/gico.pb.go:1144 +0x245
google.golang.org/grpc.(*Server).processUnaryRPC(0xc0001e2000, 0xda80c0, 0xc0001f8180, 0xc0002c2300, 0xc0001d4210, 0x1312378, 0x0, 0x0, 0x0)
	/builds/git-compose/git-compose/.go/pkg/mod/google.golang.org/grpc@v1.31.1/server.go:1180 +0x4e2
google.golang.org/grpc.(*Server).handleStream(0xc0001e2000, 0xda80c0, 0xc0001f8180, 0xc0002c2300, 0x0)
	/builds/git-compose/git-compose/.go/pkg/mod/google.golang.org/grpc@v1.31.1/server.go:1503 +0xd6b
google.golang.org/grpc.(*Server).serveStreams.func1.2(0xc000224030, 0xc0001e2000, 0xda80c0, 0xc0001f8180, 0xc0002c2300)
	/builds/git-compose/git-compose/.go/pkg/mod/google.golang.org/grpc@v1.31.1/server.go:843 +0xa6
created by google.golang.org/grpc.(*Server).serveStreams.func1
	/builds/git-compose/git-compose/.go/pkg/mod/google.golang.org/grpc@v1.31.1/server.go:841 +0x20b
```

To restore server after crash you have to restart IDE