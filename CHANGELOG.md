# Change Log

All notable changes to the "git-compose" extension will be documented in this file.

<!-- ## [Unreleased] -->

## 0.5.3

### Added
- .gico.log file under global extension storage

### Fixed
- `init` now defines current branch
- parse now works with the current (possibly unsaved) document
- gico 0.8.6: fixed when fetching with git-go was wrong and fallbacked to CLI
- gico 0.8.7: fix broken checkout - it kept old files in workdir

### Changed
- `init` is optimized to use git-go 

## 0.5.2

### Changed
- optimized gico 0.8.3 with git-go library

## 0.5.1

### Fixed
- use random port for `gico serve`

### Changed
- optimization: use webpack to make extension smaller

## 0.5.0

### Added
- added progress for `gico up`

### Changed
- optimization: run only one gico process (gico serve) and use GRPC API instead of CLI

## 0.4.1

### Fixed
- display branches when folder exists, but not a git repo

## 0.4.0

### Added
- "open" command

### Changed
- Bundled gico to latest version 0.7.0
- Now publish would always download latest bundled version
- Picture demonstrating lens in README
- Version check is more robust - we only check if Major version is 0 or 1

## 0.3.2

### Changed
- Icon.

## 0.3.1

### Fixed
- Changed misleading icon for "up" command.

## 0.3.0

### Added
- Support several git-compose.yaml files
- Up and push on each repo
- Push on all repos
- Unicode icons in lenses titles

### Fixed
- Not cloned repository breaks `up` command

## 0.2.0

### Added
- Up command on all repos via lens

## 0.1.3

### Added
- Per-repo lens with stub commands, show current branch.
- Lens with branch name.
- Pull lens.
- Bundled gico.
- Setting to switch to system gico.
- Hover help for `repos:`.