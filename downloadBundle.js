/**
 * Copyright (c) 2020 Timur Sultanaev All rights reserved.
 * Use of this source code is governed by a BSD-style
 * license that can be found in the LICENSE file.
 */

const https = require('https');
const fs = require('fs');

if (process.env.SKIP_GICO_DOWNLOAD == "true") {
    console.log("skipping download of gico bundle")
    process.exit()
}

if (!fs.existsSync("./bundled-gico")) fs.mkdirSync("./bundled-gico");
if (!fs.existsSync("./bundled-gico/linux")) fs.mkdirSync("./bundled-gico/linux");
if (!fs.existsSync("./bundled-gico/macos")) fs.mkdirSync("./bundled-gico/macos");
if (!fs.existsSync("./bundled-gico/windows")) fs.mkdirSync("./bundled-gico/windows");

function downloadBinary(os, shortOs, links, ext, tag) {
    let name = `gico_${shortOs}_${tag}`
    let url = links.filter(it => it.name === name)[0].url
    console.log("Downloading from " + url)
    const file = fs.createWriteStream(`./bundled-gico/${os}/gico${ext}`);
    const request = https.get(url, response => {
        response.pipe(file);
    });
}


https.get("https://gitlab.com/api/v4/projects/17475748/releases?per_page=1",
    (resp) => {
        let data = '';
        resp.on('data', (chunk) => {
            data += chunk;
        });
        resp.on('end', () => {
            let releases = JSON.parse(data)
            let release = releases[0]
            let tag = release.tag_name
            console.log("Have release: " + tag)
            let links = release.assets.links
            downloadBinary("linux", "linux", links, "", tag)
            downloadBinary("windows", "win", links, ".exe", tag)
            downloadBinary("macos", "mac", links, ".mac.exe", tag)
        });
    })
