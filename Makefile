package:
	SKIP_GICO_DOWNLOAD=true vsce package \
	  --baseImagesUrl \
	  https://gitlab.com/git-compose/git-compose-vscode/-/raw/master/

repackage:
	vsce package \
	  --baseImagesUrl \
	  https://gitlab.com/git-compose/git-compose-vscode/-/raw/master/

login:
	vsce login git-compose

publish:
	vsce publish \
	  --baseImagesUrl \
	  https://gitlab.com/git-compose/git-compose-vscode/-/raw/master/

publish-patch:
	vsce publish patch \
	  --baseImagesUrl \
	  https://gitlab.com/git-compose/git-compose-vscode/-/raw/master/

publish-minor:
	vsce publish minor \
	  --baseImagesUrl \
	  https://gitlab.com/git-compose/git-compose-vscode/-/raw/master/

rebuild-grpc:
	# npm rebuild grpc --runtime=electron --target=7.3.3 --target_platform=win32  --target_libc=unknown
	npm rebuild grpc --runtime=electron --target=7.3.3 --target_platform=linux
	# npm rebuild grpc --runtime=electron --target=7.3.3 --target_platform=darwin

install-tools:
	npm install
	npm install -g grpc-tools
	npm install -g grpc_tools_node_protoc_ts

gen:
	cp ../git-compose/gico_api/gico.proto src/proto/gico.proto
	grpc_tools_node_protoc \
		-I src/proto \
		--js_out=import_style=commonjs,binary:src/gen \
		--grpc_out=grpc_js:src/gen src/proto/gico.proto
	
	protoc \
		-I ./src/proto \
		--ts_out=generate_package_definition:src/gen src/proto/gico.proto

dev:
	yarn webpack-dev
